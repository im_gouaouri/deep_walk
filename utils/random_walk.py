import random
# print(__name__)


def random_walk(graph, n_walks=10, n_iterations=100):
    walks = []
    for _ in range(n_walks):
        walk_steps = []

        # random_node is the start node selected randomly
        random_node = random.choice(
            [i for i in list(graph.nodes())])

        # Traversing through the neighbors of start node
        for _ in range(n_iterations):
            list_for_nodes = list(graph.neighbors(random_node))
            if len(list_for_nodes) != 0:  # if random_node has outgoing edges
                # choose a node randomly from neighbors
                random_node = random.choice(list_for_nodes)
                walk_steps.append(random_node)
            else:
                break
        if len(walk_steps) >= 2:
            walks.append(walk_steps)
    return walks


def generate_corpus(walks):
    # taking into account that there's only one relation type
    corpus = []
    for walk in walks:
        corpus.append(" is subcategory of ".join(walk))
    return corpus
