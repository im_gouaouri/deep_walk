from neo4j import GraphDatabase
import logging
from neo4j.exceptions import ServiceUnavailable
import numpy as np
import networkx as nx
from utils.random_walk import random_walk, generate_corpus


class App:

    def __init__(self, uri, user, password):
        self.driver = GraphDatabase.driver(uri, auth=(user, password))

    def close(self):
        # Don't forget to close the driver connection when you are finished with it
        self.driver.close()

    def find_cat(self,):
        with self.driver.session() as session:
            result = session.execute_read(
                self._find_and_return_cat)
            return result

    @staticmethod
    def _find_and_return_cat(tx):
        query = (
            "MATCH (child) -[:SUB_CAT_OF]-> (c:Category) "
            "RETURN child.name as name, 'sub category of' as rel, c.name as cat_name "
        )
        result = tx.run(query)
        return [[row['name'], row['rel'], row['cat_name']] for row in result]


if __name__ == "__main__":
    # Aura queries use an encrypted connection using the "neo4j+s" URI scheme
    uri = "bolt://127.0.0.1:7687"
    user = "neo4j"
    password = "neo"
    app = App(uri, user, password)
    facts = np.array(app.find_cat())
    app.close()
    G = nx.DiGraph()
    G.add_nodes_from(facts[:, 0])
    G.add_nodes_from(facts[:, 2])
    G.add_edges_from([(h, t) for h, t in zip(facts[:, 0], facts[:, 2])])
    walks = random_walk(G, n_walks=3000, n_iterations=500)
    corpus = generate_corpus(walks)
    print(corpus)
    # print(list(G.edges()))

    # print(len(list(app.graph.nodes())))
